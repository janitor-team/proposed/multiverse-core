Source: multiverse-core
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               gradle-debian-helper,
               javahelper,
               maven-repo-helper
Standards-Version: 4.6.1
Homepage: http://multiverse.codehaus.org/
Vcs-Git: https://salsa.debian.org/java-team/multiverse-core.git
Vcs-Browser: https://salsa.debian.org/java-team/multiverse-core

Package: libmultiverse-core-java
Architecture: all
Depends: ${misc:Depends}
Description: Java library implementing Software Transactional Memory (STM)
 Multiverse is meant as an alternative to traditional lock based
 concurrency. If you have worked with databases before, Transactional
 Memory should feel familiar because both share one very important
 concept: transactions.
 .
 Multiverse is language independent so it can be used without relying
 on instrumentation and therefore can easily be used with other
 languages that can run on the JVM like Scala, Groovy or JRuby.
 .
 Also it reduces complexity of concurrency control, it can be used
 demanding production environments and provides a framework for STM
 experiments.
